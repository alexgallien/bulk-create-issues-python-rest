#!/usr/bin/env python
# v4 - feature requests, wow

import getpass
import sys
import json
import requests
import argparse

# -----------------------------------------------------------------------------
'''
Usage: Creates a predefined number of issues in a Jira project based on the variables below 

    url = URL of the instance you are hitting, REST endpoint passed automatically
    project = ID of project you want to create issues in
    username = Username to run REST call with
    password = Password of above
    num_issues = Number of issues to create
        
If plaintext password entry is not desired, use -p to enter password securely.
'''

url = 'http://172.16.56.149:8750'
project = '10000'
username = 'adminlocal'
password = 'pac1u9drogs4ib'
num_issues = 1

# -----------------------------------------------------------------------------
'''
Todo (! for things done in this version)

Major:
!- Better error handling for bad username/password
!- Define what project issues are created in.
!- Secure password entry option

Minor
- Input options?
  - Add argparser to input url/username/password 
  - YAML config file?
  - argparser flag for raw input?
- Better error handling, add a output file?
'''

# -----------------------------------------------------------------------------


class IssueCreator(object):

    def __init__(self, url, project, username, password, num_issues):

        self.url = url
        self.project = project
        self.username = username
        self.password = password
        self.num_issues = num_issues

    def write_data(self, iter):
        # JSON Payload
        data = {
            "fields": {
                "project":
                { 
                    "id": self.project
                },
                "summary": "Issue #" + str(iter),
                "description": "This is issue number " + str(iter),
                "issuetype": 
                {
                    "id": "10002"
                }
            }
        }  

        data = json.dumps(data)

        # Hitting the issue endpoint with provided url
        endpoint = self.url + '/rest/api/2/issue'

        r = requests.post(
            endpoint,
            data=data,
            auth=(self.username, self.password),
            headers={'Content-Type': 'application/json'}
        )

        status_code = r.status_code

        # Prevents trying to run the loop with bad creds causing CAPTCHA lockout
        if status_code == 401:
            sys.exit("Invalid credentials provided, please check username/password")

        # This was mostly for debugging
        # r.raise_for_status()

    def main(self):

        for iter in range(1, self.num_issues + 1):
            self.write_data(iter)


if __name__ == "__main__":

    # argparse in case you don't want to send pw in plaintext
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--man_pass",
        action='store_true',
        help="Optionally entering password in the terminal, if you don't want to store in text.")

    options = parser.parse_args()

    if options.man_pass:
        password = getpass.getpass()

    foo = IssueCreator(
        url=url,
        project=project,
        username=username,
        password=password,
        num_issues=num_issues
        )

    foo.main()