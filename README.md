### Jira Bulk Create Issues ###

A Python script to quickly create a number of issues in a project!

### How do I get set up? ###

- Install Python 2.7 (does not currently support Python 3.x)
- [Install the Requests package](http://docs.python-requests.org/en/master/user/install/)

### How do I use the script? ###

Creates a predefined number of issues in a Jira project based on the variables below:

    url = URL of the instance you are hitting, REST endpoint passed automatically
    project = ID of project you want to create issues in
    username = Username to run REST call with
    password = Password of above
    num_issues = Number of issues to create
        
If plaintext password entry is not desired, use -p to enter password securely.